<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Entity\Student;

class JSONController extends AbstractController
{
    /**
     * @Route("/json-students", name="json-students")
     */
    public function index()
    {

        $students = $this->getDoctrine()
        ->getRepository(Student::class)
        ->findAll();

        $serializer = $this->get('serializer');

        $response = $serializer->serialize($students,'json');

        return new Response($response);

    }
}
